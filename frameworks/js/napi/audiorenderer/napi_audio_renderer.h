/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NAPI_AUDIO_RENDERER_H_
#define NAPI_AUDIO_RENDERER_H_

#include <iostream>
#include <map>
#include <queue>

#include "audio_renderer.h"
#include "audio_errors.h"
#include "audio_system_manager.h"
#include "napi/native_api.h"
#include "napi/native_node_api.h"
#include "audio_stream_manager.h"
#include "napi_param_utils.h"
#include "napi_async_work.h"
#include "napi_audio_error.h"

namespace OHOS {
namespace AudioStandard {

} // namespace AudioStandard
} // namespace OHOS
#endif /* AUDIO_RENDERER_NAPI_H_ */
